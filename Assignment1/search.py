#!/usr/bin/env python3
"""
Introduction to Artificial Intelligence
Assignment 1
Tom Hollo <102577218>
"""
# main.py

from sys import argv
from ast import literal_eval
# import our algorithims 
import depth_first_search
import breadth_first_search
import greedy_best_first
import a_star
import uniform_cost
import modified_cost

def get_grid(size, walls, goals):
  grid = []
  # create an empty grid using the xy size we have
  [grid.append([0] * size[1]) for i in range(size[0])]
  # add all the walls to the grid 
  for i in range(len(walls)):
    maxY = (walls[i][1] + walls[i][3])
    minY = walls[i][1]
    for y in range(minY, maxY):
      maxX = (walls[i][0] + walls[i][2])
      minX = walls[i][0]  # add the goals to the grid
      for x in range(minX, maxX):
        grid[y][x] = 7

  if isinstance(goals[0], tuple):
    for i in range(len(goals)):
      grid[goals[i][1]][goals[i][0]] = 6
  # there is only one goal
  else:
    grid[goals[0]][goals[1]] = 6

  return grid

# nodes object will contain node objects
class node:
  def __init__(self, position):
    self.seen = False
    self.wall = False
    self.goal = False
    self.x = position[0]
    self.y = position[1]

# our algorithims will interact with nodes object
# we need, the grid array, agent location
class nodes:
  def __init__(self, grid_array, agent_loc):
    self.current_node = None
    self.nodes = []
    for y in range(len(grid_array)):
      for x in range(len(grid_array[y])):
        n = node([x, y])
        if grid_array[y][x] == 7:
          n.wall = True
        elif grid_array[y][x] == 6:
          n.goal = True
        if (x == agent_loc[0]) and (y == agent_loc[1]):
          self.current_node = n
        self.nodes.append(n)

def read(filename):
  textfile = open(filename, "r")
  
  # convert grid to an array format [x,y] 
  grid = textfile.readline()
  grid = literal_eval(grid)

  # convert agent location to an array format [x, y]
  a_l = textfile.readline()
  a_l = [int(s) for s in a_l if s.isdigit()]

  # convert goal states to a multidimensional array 
  g_s = textfile.readline()
  g_s = g_s.replace("|", ",")
  g_s = literal_eval(g_s)
  
  # convert the walls into a usable data structure 
  walls = [literal_eval(wall) for wall in textfile] 
  # size, location, goals, walls
  return [grid, a_l, g_s, walls]

def main():
  # take in command line arguments 
  script, filename, method = argv
  _file = read(filename)
  # size, walls, goals
  _grid = get_grid(_file[0], _file[3], _file[2])
  # grid_array, location
  ns = nodes(_grid, _file[1])
  # ns is the object that our algorithms will use to search
  if method == "dfs":
    depth_first_search._dfs(ns, [], filename, 1)
  elif method == "bfs":
    breadth_first_search.bfs(ns, filename)
  elif method == "gbfs":
    greedy_best_first.gbs(ns, filename)
  elif method == "as":
    a_star.a_star_search(ns, filename)
  elif method == "ucs":
    uniform_cost.uni_cost(ns, filename)
  elif method == "mcs":
    modified_cost.uni_cost(ns, filename)
  else:
    print("do not recognise that method")

if __name__ == "__main__":
  main()
