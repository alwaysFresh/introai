# depth_first_search.py

from utility_functions import nextNodes
from utility_functions import print_output

# my implementation of depth first search
def _dfs(_nodes, prev_nodes, filename, tree_size, moves=[]):
  # mark current node as seen
  _nodes.current_node.seen = True
  next_nodes, output = nextNodes(_nodes.nodes, _nodes.current_node)

  # look through next options
  for n, o in zip(next_nodes, output):
  # if node is goal, return
    if n.goal is True:
      moves.append(o)
      # filename, method, path
      print_output(filename, " Depth First Search ", moves, tree_size)
      _nodes.current_nodes = n
      exit()
  # else if node is wall, continue
    elif n.wall is True:
      continue
  # else if node has not been visited 
    elif n.seen is False:
      # store the previous node in case we need to backtrack 
      tree_size+=1
      moves.append(o)
      prev_nodes.append(_nodes.current_node)
      _nodes.current_node = n
      _dfs(_nodes, prev_nodes, filename, tree_size)

  # else revert back to previous node and twy again 
  if (prev_nodes is not None):
    index = len(prev_nodes) - 1
    _nodes.current_node = prev_nodes[index]
    # remove node from list so we dont get stuck
    moves.pop()
    prev_nodes.pop()
    _dfs(_nodes, prev_nodes, filename, tree_size)

  print("could not find the goal") 


