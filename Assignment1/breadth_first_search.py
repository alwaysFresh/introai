# breadth_first_search.py
from utility_functions import nextNodes
from utility_functions import print_output

def bfs(_nodes, filename):
  # mark the current node as seen
  _nodes.current_node.seen = True
  # add current node to a queue
  queue  = []
  queue.append(_nodes.current_node)
  # while queue is not empty
  result = [["start"]]
  tree_size = 1
  while queue is not None:
    # current node is top of queue
    _nodes.current_node = queue[0] # queue.top()
    # remove current node from queue 
    queue.pop(0) # first item in the queue 
    # for the next nodes from current node, 
    next_nodes, output = nextNodes(_nodes.nodes, _nodes.current_node)
    # remove path from result, create a new one for each of the possible next 
    # nodes and add it back to the end of result 
    path = result.pop(0)
    for n, o in zip(next_nodes, output):
      # if next node is goal
      if n.goal is True:
        path.append(o)
        print_output(filename, " breadth first search ", path[1:], tree_size)
        exit()
      # if next node is wall, pass
      elif n.wall is True:
        pass
      # if next node has not been seen
      elif n.seen is False:
        tree_size += 1
        n.seen = True
        queue.append(n)
        new_path = list(path)
        new_path.append(o)
        result.append(new_path)

  print("could not find the goal")
