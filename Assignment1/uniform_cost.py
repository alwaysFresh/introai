# uniform_cost.py
# this is effectively the same as breadth_first_search, as each node has the same cost
# we change this in the research part of the report 
from utility_functions import PriorityQueue
from utility_functions import nextNodes
from utility_functions import backtrack
from utility_functions import cost
from utility_functions import modified_cost
from utility_functions import print_output

def uni_cost(_nodes, filename):
  # basically breadth_first_search but it checks cost
  start = _nodes.current_node
  # mark current node as seen
  _nodes.current_node.seen = True
  # initialise dictionaries to track self
  came_from = {}
  came_from[_nodes.current_node] = None
  cost_so_far = {}
  cost_so_far[_nodes.current_node] = 0
  # initialise queue
  queue = PriorityQueue()
  # push current/starting node to source
  queue.insert(0, _nodes.current_node)

  tree_size = 1
  # while the queue is not empty
  while queue is not None:
    # pop node from queue 
    current = queue.get()
    next_nodes, output = nextNodes(_nodes.nodes, current)
    # check if current node is our goal
    if current.goal is True:
      # do things 
      moves = backtrack([start.x, start.y], came_from, current)
      print_output(filename, " uniform cost ", moves, tree_size)
      exit()
    # loop through adjacent nodes 
    for n in next_nodes:  
      # get new cost by adding the cost_so_far to cost(node) 
      new_cost = cost_so_far[current] + cost((current.x, current.y), (n.x, n.y))
      if n.wall is True:  
        # continue, we don't touch this pos
        continue
      # if node is not seen( eg. if n not in cost_so_far ) and new_cost < cost_so_far
      if n not in cost_so_far or new_cost < cost_so_far[n]:  
        tree_size += 1
        cost_so_far[n] = new_cost
        # insert the node into the queue with cost as the priority
        queue.insert(new_cost, n)
        # mark the current node as the node that n(ode) came from
        came_from[n] = current
        
  print("could not find the goal")

