# a_star.py

from utility_functions import print_output
from utility_functions import nextNodes
from utility_functions import heuristic
from utility_functions import PriorityQueue 
from utility_functions import backtrack 
from utility_functions import get_goal
from utility_functions import cost

def a_star_search(_nodes, filename):
  start = _nodes.current_node
  goal = get_goal(_nodes.nodes, _nodes.current_node)
  queue = PriorityQueue()
  queue.insert(0, _nodes.current_node)
  tree_size = 1
  came_from = {}
  came_from[_nodes.current_node] = None

  # in A* we add a cost
  cost_so_far = {}
  cost_so_far[_nodes.current_node] = 0

  while queue is not None:
    current = queue.get()

    next_nodes, output = nextNodes(_nodes.nodes, current)
    if current.goal is True:
      moves = backtrack([start.x, start.y], came_from, current)
      print_output(filename, " A* ", moves, tree_size)
      exit()

    for n in next_nodes:
      new_cost = cost_so_far[current] + cost((current.x, current.y), (n.x, n.y))

      if n.wall is True:
        continue
      if n not in cost_so_far or new_cost < cost_so_far[n]:
        tree_size += 1
        # we don't need to set the node to seen, because we just check if its in 
        # cost_so_far
        cost_so_far[n] = new_cost
        priority = new_cost + heuristic(goal, [n.x, n.y])
        queue.insert(priority, n)
        came_from[n] = current

  print("could not find the goal")

