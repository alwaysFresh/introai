# utility_functions.py

# --------------------------------------------------------------
# loop through the nodes, return a list with the 4 adjacent positions 
# with the priority Up left, down, right 
def nextNodes(nodes, current):
  result = []
  output = []
  change = -1
  check = current.y

  # up
  for node in nodes:
    if (node.x == current.x) and (node.y == current.y - 1):
      result.append(node)
      output.append("up; ")
  # left
  for node in nodes:
    if (node.x == current.x - 1) and (node.y == current.y):
      result.append(node)
      output.append("left; ")
  # down
  for node in nodes:
    if (node.x == current.x) and (node.y == current.y + 1):
      result.append(node)
      output.append("down; ")
  # right
  for node in nodes:
    if (node.x == current.x + 1) and (node.y == current.y):
      result.append(node)
      output.append("right; ")
              
  return result, output

# --------------------------------------------------------------
class PriorityQueue:
  def __init__(self):
    self.contents = []

  def insert(self, priority, location):
    self.contents.append((priority, location))
  
  def get(self):
    m = None
    for i in self.contents:
      if m is None:
        m = i
      elif m[0] > i[0]:
        m = i
    self.contents.remove(m)
    return m[1]

  def empty(self):
    if self.contents is None:
      return True

    return False

# --------------------------------------------------------------
def heuristic(a, b):
  (x1, y1) = a
  (x2, y2) = b
  return abs(x1 - x2) + abs(y1 - y2)

# --------------------------------------------------------------
# find the optimal goal node to conduct the search with
def get_goal(nodes, start):
  goals = []
  for node in nodes:
    if node.goal is True:
      goals.append(node)
  
  goal_score = heuristic([start.x, start.y], [goals[0].x, goals[0].y])
  goal = goals[0]
  for g in goals:
    g_score = heuristic([start.x, start.y], [g.x, g.y])
    if g_score < goal_score:
      goal_score = g_score
      goal = g
  
  return [goal.x, goal.y]


# --------------------------------------------------------------
def backtrack(start, came_from, current):
  move_dict = {
    "x-1": "left; ",
    "x1": "right; ",
    "y-1": "up; ",
    "y1": "down; "
    }

  result = []
  
  prev = came_from[current]

  while [current.x, current.y] != start: 
    x_diff = current.x - prev.x
    y_diff = current.y - prev.y

    if x_diff != 0:
      result.append(move_dict["x" + str(x_diff)])
      current = prev
      prev = came_from[current]
    elif y_diff != 0:
      result.append(move_dict["y" + str(y_diff)])
      current = prev
      prev = came_from[current]
    else:
      print("something is very wrong here", x_diff, y_diff)

  # since we started from the end node we need to reverse the directions
  result.reverse()
  return result
# --------------------------------------------------------------
def print_output(filename, method, path, nodes):
  path_string = "" 

  # print the filename, method and nodes
  print(filename + method + str(nodes))
  # print path 
  for p in path:
    path_string += p
  print(path_string)

# --------------------------------------------------------------
def cost(start, current):
  # subtract the current xy from start xy and 
  (x1, y1) = start
  (x2, y2) = current
  return abs(x1 - x2) + abs(y1 - y2)
# the cost function we explored with uniform_cost for research
def modified_cost(start, current):
  key = {
    1: [2, 4],
    -1: [2, 1],
    0: [0, 0]
    }

  (x1, y1) = start
  (x2, y2) = current
  return key[x1 - x2][0] + key[y1 - y2][1]

