# greedy_best_first.py

from utility_functions import nextNodes
from utility_functions import print_output
from utility_functions import PriorityQueue
from utility_functions import heuristic
from utility_functions import get_goal
from utility_functions import backtrack

def gbs(_nodes, filename):
  # calculate which goal node is closest
  goal = get_goal(_nodes.nodes, _nodes.current_node)
  start = _nodes.current_node
  # initialise queue
  queue = PriorityQueue()
  queue.insert(0, _nodes.current_node)
  # initialise backtrack dictionary
  came_from = {}
  came_from[_nodes.current_node] = None

  tree_size = 1

  while queue is not None:
    # store our current locally because I ceebs typing the whole object
    current = queue.get()
    next_nodes, output = nextNodes(_nodes.nodes, current)


    if current.goal is True:
      moves = backtrack([start.x, start.y], came_from, current)
      print_output(filename, " greedy_best_first_search ", moves, tree_size) 
      quit()

    for n, o in zip(next_nodes, output):
      if n.wall is True:
        continue
      
      if n.seen is False:
        tree_size += 1
        n.seen = True
        priority = heuristic(goal, [n.x, n.y])
        queue.insert(priority, n)
        # we will try and backtrack with lists instead of our node objects
        came_from[n] = current
  
  print("could not find the goal")

