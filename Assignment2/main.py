#!/usr/bin/env python3
#main.py

from sys import argv
import forward_chaining
import backward_chaining
import truth_table

def read(filename):
  lines = []
  with open(filename) as input_file:
    for i, line in enumerate(input_file):
      lines.append(line.rstrip("\n"))
  input_file.close()

  # now we get the lines we need
  for i in range(len(lines)):
    if lines[i] == 'TELL':
      rule_base = lines[i + 1].replace(" ", '').split(";")
      rule_base.pop()
    if lines[i] == 'ASK':
      proposition = lines[i + 1]

  #print(rule_base, proposition)
  return rule_base, proposition

# constructs a version of the rule_base the computer can read
def structure(rules):
  ret = []
  for rule in rules:
    s1 = rule.split("=>")
    ret.append([i.split("&") for i in s1]) 
  return ret

def output(success, symbols):
  output = success 
  if output == "NO":
    pass
  else:
    comma = True
    for i, s in enumerate(symbols):
      if i == len(symbols) - 1:
        comma = False
      if comma:
        output += s + ", "
      else:
        output += s

  return output 

def main():
  script, method, filename = argv
  rule_base, proposition = read(filename)
  rules = structure(rule_base)

  # check which method to execute
  if method == "FC":
    symbols, success  = forward_chaining.execute(rules, proposition)
    print(output(success, symbols))
  elif method == "BC":
    symbols, success = backward_chaining.execute(rules, proposition) 
    print(output(success, symbols))
  elif method == "TT":
    success = truth_table.execute(rules, proposition)
    print(success)


if __name__ == "__main__":
  main()
