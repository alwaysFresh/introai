# backward_chaining.py
from forward_chaining import create_database

def execute(rules, proposition):
  result = "NO"
  ret = [proposition]
  rules = sorted(rules, key=len)
  knowledge = create_database(rules)
  finished = False

  initial_facts = get_initial_facts(rules)
  # remove initial facts from rules
  rules = [r for r in rules if len(r) > 1]
  knowledge[proposition] = True

  while not finished:
    count = len(ret)
    for rule in rules:
      items, istrue = checkRule(rule, knowledge)
      if istrue:
        for item in items:
          knowledge[item] = istrue
          if item not in ret:
            ret.append(item)
          # if all the items in intial facts are true, we can end
          if any([knowledge[i] for i in initial_facts]): 
            result = "YES: "
            finished = True
            #break
    if count == len(ret):
      break
    
  ret.reverse()
  return ret, result
  
def get_initial_facts(rules):
  ret = []
  for rule in rules:
    #print(rule)
    if len(rule) == 1:
      ret.append(rule[0][0])

  return ret

# returns the first item in rule, returns true if the first index items
# are true based on if the second item as a key in knowledge is true
def checkRule(rule, knowledge):
  ret = False
  item = rule[0]
  condit = rule[1][0] 
  if knowledge[condit]:
    ret = True
  return item, ret 

