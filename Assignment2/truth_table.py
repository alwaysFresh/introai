# truth_table.py
from forward_chaining import create_database
import copy

# global variable for no. of models
n_true = 0

def execute(rules, proposition):
  symbols = list(create_database(rules))
  # precaution
  if proposition not in symbols:
    symbols.append(proposition)
  
  ret = "NO"
  result = tt_check_all(rules, proposition, symbols, {})
  global n_true
  if result:
    ret = "YES: " + str(n_true)
  n_true = 0 # have to reset since I'm doing unit tests
    
  return ret

def tt_check_all(kb, alpha, symbols, model):
  if not symbols:
    if pl_true(kb, model):
      if pl_true(alpha, model):
        global n_true 
        n_true += 1
      return pl_true(alpha, model)
    # else
    return True
  else:
    # all but first symbol
    rest = symbols[1:]
    # first symbol
    p = symbols[0]
    # make copies of model
    true_model = extend(p, True, model)
    false_model = extend(p, False, model)

    return tt_check_all(kb, alpha, rest, true_model) and tt_check_all(kb, alpha, rest, false_model)
  
def implication(a, b):
  return not(a) or b

def extend(p, b, model):
  ret = copy.deepcopy(model) 
  ret[p] = b
  return ret

def pl_true(sentence, model):
  ret = True
  if isinstance(sentence, str):
    ret = model[sentence]
  elif isinstance(sentence, list):
    for c in sentence:
      if len(c) == 1: # is a fact
        args = c[0]
        if not pl_true(args[0], model):
          ret = False
      else:
        args = c[0]
        imp = c[1]
        a_true = all([pl_true(arg, model) for arg in args])
        i_true = all([pl_true(i, model) for i in imp])
        if not implication(a_true, i_true):
          ret = False 

  return ret 

def pl_true_three(sentence, model):
  if isinstance(sentence, str):
    return model[sentence]
  elif isinstance(sentence, list):
    for c in sentence:
      if len(c) == 1: # is a fact
        args = c[0]
        print(args[0])
        if not pl_true_three(args[0], model):
          return False
      else:
        args = c[0]
        imp = c[1]
        a_true = all([pl_true_three(arg, model) for arg in args])
        i_true = all([pl_true_three(i, model) for i in imp])
        #print(not(a_true) or i_true)
        print(args,"=>", imp)
        if not implication(a_true, i_true):
          return False 

  return True


