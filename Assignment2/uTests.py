#!/usr/bin/env python3
# unit tests boi
import unittest
import main
import forward_chaining
import backward_chaining
import truth_table

def prep_search(filename):
  rule_base, proposition = main.read(filename) 
  rules = main.structure(rule_base)
  return rules, proposition

def run_forward_chain(filename):
  rules, proposition = prep_search(filename)
  symbols, success = forward_chaining.execute(rules, proposition)
  output = main.output(success, symbols) 
  return output

def run_backward_chain(filename):
  rules, proposition = prep_search(filename)
  symbols, success = backward_chaining.execute(rules, proposition)
  output = main.output(success, symbols) 
  return output

def run_truth_table(filename):
  rules, proposition = prep_search(filename)
  success = truth_table.execute(rules, proposition)
  output = success
  return output

class TestStringMethods(unittest.TestCase):
  # forward chain tests
  def test_fc_1(self):
    output = run_forward_chain("tests/test1.txt")
    self.assertEqual(output, "YES: a, b, p2, p3, p1, d")
  def test_fc_2(self):
    output = run_forward_chain("tests/test2.txt")
    self.assertEqual(output, "YES: croaks, flies, frog, green")
  def test_fc_3(self):
    output = run_forward_chain("tests/test3.txt")
    self.assertEqual(output, "YES: a, b, p3, p1, d")
  def test_fc_4(self):
    output = run_forward_chain("tests/test4.txt")
    self.assertEqual(output, "YES: a, e, d, b, c")
  def test_impossible_test_fc5(self):
    output = run_forward_chain("tests/test5.txt")
    self.assertEqual(output, "NO")
  # backward_chain tests
  # TODO: are we meant to include all of the "initial facts" i don't think so but maybe
  def test_bc_1(self):
    output = run_backward_chain("tests/test1.txt")
    self.assertEqual(output, "YES: p2, p3, p1, d")
  def test_bc_2(self):
    output = run_backward_chain("tests/test2.txt")
    self.assertEqual(output, "YES: croaks, frog, green")
  def test_bc_3(self):
    output = run_backward_chain("tests/test3.txt")
    self.assertEqual(output, "YES: p3, p1, d")
  def test_bc_4(self):
    output = run_backward_chain("tests/test4.txt")
    self.assertEqual(output, "YES: e, b, c")
  def test_bc_4(self):
    output = run_backward_chain("tests/test5.txt")
    self.assertEqual(output, "NO")
  def test_bc_long(self):
    output = run_backward_chain("tests/test_lecture.txt")
    self.assertEqual(output, "YES: A, M, L, P, Q")
  # TRUTH TABLE tests
  def test_tt_1(self):
    # test 69 should be 1
    output = run_truth_table("tests/test69.txt")
    self.assertEqual(output, "YES: 1")
  def test_tt_2(self):
    # test 2 should be 10
    output = run_truth_table("tests/test2.txt")
    self.assertEqual(output, "YES: 10")
  def test_tt_3(self):
    # test 1 should be 3
    output = run_truth_table("tests/test1.txt")
    self.assertEqual(output, "YES: 3")
  def test_impossible_tt(self):
    output = run_truth_table("tests/test5.txt")
    self.assertEqual(output, 'NO')

if __name__ == "__main__":
  unittest.main()
