#forward_chaining.py
# TODO: check the textbook pages he mentions in the assignment pdf, maybe they will help
# with the above TODO

def execute(rules, proposition):
  ret = []
  result = "NO"
  rules = sorted(rules, key=len)
  knowledge = create_database(rules)
  finished = False 
  while finished is False:
    # we check this at the end to see if we should stop
    count = len(ret)
    # loop through rules
    for rule in rules:
      # if every item in first element of rule is true (use knowledge to check)
      item, istrue = checkRule(rule, knowledge) 
      knowledge[item] = istrue
      if istrue:
        if item not in ret:
          ret.append(item)
        # if second item in rule is proposition
        if item == proposition:
          result = "YES: "
          finished = True
          break
    # if count is the same, there are no more truths to be discovered and looping anymore would
    # be a waste
    if count == len(ret):
      break

  return ret, result 
    
def checkRule(rule, knowledge):
  ret = False
  # first we check single item statements
  if len(rule) == 1:
    item = rule[0]
    ret = True
  # otherwise, we compare against what we already know
  else:
    condition = rule[0]
    item = rule[1]
    for i in condition:
      ret = knowledge[i] 
      
  return item[0], ret

def create_database(rules):
  ret = {}
  for rule in rules:
    for items in rule:
      for item in items:
        if item not in ret:
          ret[item] = False
  return ret

