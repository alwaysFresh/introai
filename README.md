# README #

### What is this repository for? ###

* a place for COS30019 Introduction to AI Tutors to review my progress on assignments 
* Version 1.0 (not super relevant)

### How do I get set up? ###

* run in terminal with python3 interpreter (I am using python 3.7.3)
* method arguments are "depth_first_search" and "breadth_first_search" (without the quotes)
* Dependencies: imports sys and ast (for argument and file processing), should not have to install these they should come 
  with python (I will check that they do before submission)
* To run type "python3 main.py <filename> <method>" (method as specified above)

### Who do I talk to? ###

* Repo owner or admin
